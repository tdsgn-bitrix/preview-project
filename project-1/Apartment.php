<?
	class Apartment
	{
		public $iblock_id = 2;
		public $apartment_id;
		public $apartment;
		function __construct($apartment_id)
		{
			$this->apartment_id = $apartment_id;
			CModule::IncludeModule("iblock");
		}

		function getApartmentById()
		{
			// получаем квартиру
			$arSelect = Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_WORK_ID", "PREVIEW_PICTURE", "PROPERTY_*");
			$arFilter = Array("IBLOCK_ID" => $this->iblock_id, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "ID" => $this->apartment_id);
			$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
			while($ob = $res->GetNextElement())
			{
			 	$arFields = $ob->GetFields();
			 	$arProps = $ob->GetProperties();
			 	$this->apartment = $arFields;
			 	foreach ($arProps as $arProp)
			 	{
			 		$this->apartment['PROPS'][$arProp['NAME']] = $arProp['VALUE'];
			 	}
			}
			$this->apartment['PHOTOS'] = self::getPhotos();
			return $result;
		}

		private function getPhotos()
		{
			$VALUES = array();
		    $res = CIBlockElement::GetProperty($this->iblock_id, $this->apartment_id, "sort", "asc", array("CODE" => "MORE_PHOTO"));
		    while ($ob = $res->GetNext())
		    {
		        $VALUES[] = $ob['VALUE'];
		    }
		    return $VALUES;
		}

		public function generate_pdf()
		{
			$html = self::getHtml(); // получаем html
			$pdf=new FPDF(); 
			$mpdf = new mPDF('utf-8', 'A4', '8', '', 10, 10, 7, 7, 10, 10);
			$mpdf->charset_in = 'cp1251';
			$stylesheet = file_get_contents('style.css');
			$mpdf->WriteHTML($stylesheet, 1);
			$mpdf->list_indent_first_level = 0;
			$mpdf->WriteHTML($html, 2);
			$mpdf->Output('mpdf.pdf', 'I');
			print $html;
			$pdf = new FPDF('L');
			$pdf->AddFont('Tahoma', '','tahoma.php');
			$pdf->AddPage();

		}

		private function getHtml()
		{
			$apartment = $this->apartment;
			self::getApartmentById();
			$html = '<h1>' . $this->apartment['NAME'] . '</h1>';
			if (!empty($this->apartment['PROPS']))
			{
				$html .= '<ul class="props">';
				foreach ($this->apartment['PROPS'] as $propName => $prop)
					$html .= '<li><b>' . $propName . '</b> ' . $prop . '</li>';
				$html .= '</ul>';
			}
			if (!empty($this->apartment['PROPS']))
			{
				$html .= '<div class="photos">';
				foreach ($this->apartment['PHOTOS'] as $arPhoto)
					$html .= '<p><img src="' . CFILE::GetPath($arPhoto) . '" /></p>';
				$html .= '</div>';
			}
			return $html;
		}
	}
?>