<?
	class ApartmentImport
	{
		public $iblock_id_apartment = 2;
		public $iblock_id_houses = 11;
		public $path_to_import_file = '/upload/catalog_1c/FreePlaces_2.txt';
		public $second_str = ';1;ОАО "";';
		public $first_field_for_third_str = '06;;;';
		public $last_house_id = '';
		public $house_code = '';
		public $field_house = array(
		    0 => '',
			1 => 'CODE', // Код дома
			2 => 'ADDRESS', // Адрес
			3 => 'Плановая дата сдачи', // плановая дата сдачи
			4 => 'MAX_FLOOR', // Максимальный этаж
			5 => 'APARTMENTS', // Количество выводимых квартир
			6 => '',

		);

        public $fields_apartment = array(
            0 => '',
            1 => '',
            2 => '',
            3 => 'NUMBER', // номер квартиры
            4 => 'FLOOR', // этаж
            5 => 'ROOMS', // кол-во комнат
            6 => 'TOTAL_AREA', // общая площадь
            7 => 'LIVE_AREA', // жилая площадь
            8 => 'KITCHEN', // площадь кухни
            9 => 'OTDELKA', // отделка
            10 => 'NAME_LP', // Имя ЛП
            11 => '',
            12 => 'PRICE_FOR_METER', // цена за кв метр
            13 => 'PRICE', // стоимость квартиры
            14 => 'PRICE_DISCOUNT', // стоимость квартиры со скидкой
            15 => 'NAME_PLAN', // имя планировки
            16 => 'TYPE_ON', // Тип ОН
            17 => 'KEY', // значение по отделке

        );
		public $type_on = array(
			3 => 'Квартира',
			1 => 'Коттедж',
			2 => 'Офис',
			4 => 'Гараж'
		);
		public function __construct()
		{
			CModule::IncludeModule("iblock");
			$this->loadFile();
		}

		private function loadFile()
		{
			if (($handle = fopen($_SERVER['DOCUMENT_ROOT'] . $this->path_to_import_file, "r")) !== FALSE) {
				$this->clean_apartments();
				$row = 1;
			    while (($data = fgetcsv($handle, 1000, ';')) !== FALSE) {
			    	$num_field  = count($data);

			    	switch ($num_field){
			    		case 1:
			    		break;
			    		case 4:
			    		break;
			    		case 7:
			    			$this->addHouse($data);
			    		break;
			    		case 18:
			    			$this->addApartment($data);
			    		break;
			    		default: 
			    		break;
					}
			    }
			    fclose($handle);
			    unlink($_SERVER['DOCUMENT_ROOT'] . $this->path_to_import_file);
			}
			return;
		}

		private function addHouse($data)
		{
			$el = new CIBlockElement;
			$PROP = array(
				'NUMBER' => $data[2],
				'DATE_COMPLETE' => $data[3],
				'MAX_FLOOR' => $data[4],
				'APARTMENTS' => $data[5]
			);
			$arLoadProductArray = Array(
			  "IBLOCK_SECTION_ID" => false,
			  "IBLOCK_ID"      => $this->iblock_id_houses,
			  "PROPERTY_VALUES"=> $PROP,
			  "NAME"           => $data[2],
			  "ACTIVE"         => "Y",
			  "PREVIEW_TEXT"   => $data[2],
			  "ACTIVE_FROM"	 => ConvertTimeStamp(time(), 'FULL'),
			);

            if($HOUSE_ID = $el->Add($arLoadProductArray)) {
                $this->last_house_id = $HOUSE_ID;
                $this->house_code = $data[1];
                echo "Выгрузка закончена <br> Дом: " . $data[2] . " <br> ";
            }
            else
                echo " Ошибка: " . $el->LAST_ERROR . " <br>";
            return;
		}
		private function addApartment($data)
		{
			$el = new CIBlockElement;
			$PROP = array(
				'NUMBER' => $data[3],  // номер квартиры
				'FLOOR' => $data[4],  // этаж
				'ROOMS' => $data[5],  // кол-во комнат
				'TOTAL_AREA' => str_replace(",",".",$data[6]),  // общая площадь
				'LIVE_AREA' => $data[7],  // жилая площадь
				'KITCHEN' => $data[8],  // площадь кухни
				'OTDELKA' => $data[9],  // отделка
				'NAME_LP' => $data[10],  // Имя ЛП
				'PRICE_FOR_METER' => $data[12],  // цена за кв метр
				'PRICE' => $data[13],  // стоимость квартиры
				'PRICE_DISCOUNT' => $data[14],  // стоимость квартиры со скидкой
				'NAME_PLAN' => $this->house_code . "_" . $data[15],  // имя планировки
				'TYPE_ON' => $this->type_on[$data[16]],  // Тип ОН
				'KEY' => $data[17],  // значение по отделке
				'HOUSE' => $this->last_house_id
			);
			$arLoadProductArray = Array(
			  "IBLOCK_SECTION_ID" => false,
			  "IBLOCK_ID"      => $this->iblock_id_apartment,
			  "PROPERTY_VALUES"=> $PROP,
			  "NAME"           => $data[3],
			  "ACTIVE"         => "Y",
			  "ACTIVE_FROM"	 => ConvertTimeStamp(time(), 'FULL'),
			);
            if($APARTMENT_ID = $el->Add($arLoadProductArray)){
                echo "Квартира: ". $data[3] . " <br> ";}
            else {
                echo " Ошибка: " . $el->LAST_ERROR . " <br>";
            }
		}

		private function clean_apartments()
		{
			$arSelect = Array("ID");
			$arFilter = Array("IBLOCK_ID"=>$this->iblock_id_apartment);
			$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>5000), $arSelect);
			while($ob = $res->GetNextElement())
			{
			 $arFields = $ob->GetFields();
			 CIBlockElement::Delete($arFields['ID']);
			}
			$arFilter = Array("IBLOCK_ID"=>$this->iblock_id_houses);
			$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>5000), $arSelect);
			while($ob = $res->GetNextElement())
			{
			 $arFields = $ob->GetFields();
			 CIBlockElement::Delete($arFields['ID']);
			}
			return;
		}
	}
?>