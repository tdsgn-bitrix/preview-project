<?
	class ApartmentFilter
	{
		public $iblock_id_apartment = 2;
		public $iblock_id_houses = 11;
		public $filter = array();
		public function __construct()
		{
			CModule::IncludeModule("iblock");
			$this->filter = $this->renderFilter();
		}

		private function renderFilter()
		{
			$filter['houses'] = $this->getHouses();
			$filter['prices'] = $this->getPrices();
			$filter['total_area'] = $this->getTotalArea();
			$filter['rooms'] = $this->getRooms();
			$filter['floors'] = $this->getFloors();
			return $filter;
		}
		private function getHouses()
		{
			$arSelect = Array("ID", "NAME", "PREVIEW_TEXT","PROPERTY_DATE_COMPLETE");
			$arFilter = Array("IBLOCK_ID"=>$this->iblock_id_houses, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "!PROPERTY_DATE_COMPLETE" => false);
			$res = CIBlockElement::GetList(Array('SORT' => 'ASC'), $arFilter, false, Array("nPageSize"=>50), $arSelect);
			while($ob = $res->GetNextElement())
			{
				$arFields = $ob->GetFields();
				$houses[$arFields['ID']] = array(
				 	'NAME' => $arFields['NAME'],
				 	'STREET' => $arFields['PREVIEW_TEXT'],
				 	'DATE_COMPLETE' => $arFields["PROPERTY_DATE_COMPLETE_VALUE"]
				);
			}
			ksort($houses);
			return $houses;
		}

		private function getPrices()
		{
			// минимальная цена
			$arSelect = Array("ID", "NAME", "PROPERTY_PRICE");
			$arFilter = Array("IBLOCK_ID"=>$this->iblock_id_apartment, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "!PROPERTY_PRICE" => false);
			$res = CIBlockElement::GetList(Array('PROPERTY_PRICE' => 'ASC'), $arFilter, false, Array("nPageSize"=>1), $arSelect);
			while($ob = $res->GetNextElement())
			{
				$arFields = $ob->GetFields();
				$minPrice = $arFields['PROPERTY_PRICE_VALUE'];
			}
			// максимальная цена
			$arSelect = Array("ID", "NAME", "PROPERTY_PRICE");
			$arFilter = Array("IBLOCK_ID"=>$this->iblock_id_apartment, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "!PROPERTY_PRICE" => false);
			$res = CIBlockElement::GetList(Array('PROPERTY_PRICE' => 'DESC'), $arFilter, false, Array("nPageSize"=>1), $arSelect);
			while($ob = $res->GetNextElement())
			{
				$arFields = $ob->GetFields();
				$maxPrice = $arFields['PROPERTY_PRICE_VALUE'];
			}
			return $prices = array(
				'minPrice' => $minPrice,
				'maxPrice' => $maxPrice
			);
		}

		private function getTotalArea()
		{
			// минимальная цена
			$arSelect = Array("ID", "NAME", "PROPERTY_PRICE");
			$arFilter = Array("IBLOCK_ID"=>$this->iblock_id_apartment, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "!PROPERTY_TOTAL_AREA" => false);
			$res = CIBlockElement::GetList(Array('PROPERTY_TOTAL_AREA' => 'ASC'), $arFilter, false, Array("nPageSize"=>1), $arSelect);
			while($ob = $res->GetNextElement())
			{
				$arFields = $ob->GetFields();
				$minArea = $arFields['PROPERTY_TOTAL_AREA_VALUE'];
			}
			// максимальная цена
			$arSelect = Array("ID", "NAME", "PROPERTY_PRICE");
			$arFilter = Array("IBLOCK_ID"=>$this->iblock_id_apartment, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "!PROPERTY_TOTAL_AREA" => false);
			$res = CIBlockElement::GetList(Array('PROPERTY_TOTAL_AREA' => 'DESC'), $arFilter, false, Array("nPageSize"=>1), $arSelect);
			while($ob = $res->GetNextElement())
			{
				$arFields = $ob->GetFields();
				$maxArea = $arFields['PROPERTY_TOTAL_AREA_VALUE'];
			}
            return array(
                'minArea' => $minArea,
                'maxArea' => $maxArea
            );
		}

		private function getRooms()
		{
			// получаем комнаты
			$arSelect = Array("ID", "PROPERTY_ROOMS");
			$arFilter = Array("IBLOCK_ID"=>$this->iblock_id_apartment, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
			$res = CIBlockElement::GetList(Array('ID' => 'ASC'), $arFilter, false, Array("nPageSize"=>100), $arSelect);
			while($ob = $res->GetNextElement())
			{
				$arFields = $ob->GetFields();
				$arRooms[$arFields['PROPERTY_ROOMS_VALUE']] = $arFields['PROPERTY_ROOMS_VALUE'];
				/*if ($arFields['PROPERTY_ROOMS_ENUM_ID'])
					$arRooms[$arFields['PROPERTY_ROOMS_ENUM_ID']] = $arFields['PROPERTY_ROOMS_VALUE'];*/
			}
			asort($arRooms);
			return $arRooms;
		}

		private function getFloors()
		{
			// получаем этажи
			$arSelect = Array("ID", "PROPERTY_FLOOR");
			$arFilter = Array("IBLOCK_ID"=>$this->iblock_id_apartment, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
			$res = CIBlockElement::GetList(Array('ID' => 'ASC'), $arFilter, false, Array("nPageSize"=>100), $arSelect);
			while($ob = $res->GetNextElement())
			{
				$arFields = $ob->GetFields();
				$arFloors[$arFields['PROPERTY_FLOOR_VALUE']] = $arFields['PROPERTY_FLOOR_VALUE'];
			}
			asort($arFloors);
			return $arFloors;
		}
		
	}
?>