<?
	class Apartment
	{
		public $iblock_id = 2;
		public $iblock_id_plan = 13;
		public $iblock_id_map = 15;
		public $iblock_id_contacts = 5;
		public $apartment = array();
		public $deleted_props = array(
			'BOOKED',
			'NAME_LP',
			'ROOM',
			'TOILET',
			'PRICE_DISCOUNT',
			'PRICE_FOR_METER',
			'TYPE_ON'
		);
		public function __construct()
		{
			CModule::IncludeModule("iblock");
		}
		public function getApartment($id)
		{
			$arSelect = Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE", "PROPERTY_*");
			$arFilter = Array("IBLOCK_ID"=>$this->iblock_id, "ID" => $id, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
			$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
			while($ob = $res->GetNextElement())
			{
			 $arFields = $ob->GetFields();
			 $arProps = $ob->GetProperties();

			 $this->apartment = $arFields;
			 foreach ($arProps as $propCode => $arProp)
			 	if ($propCode != 'BOOKED' && !in_array($propCode, $this->deleted_props))
			 	{
			 		if ($propCode == 'HOUSE')
			 			$this->house_id = $arProp['VALUE'];
			 		elseif ($propCode == 'NAME_PLAN')
			 			$this->apartment['PLAN'] = self::getPlan($arProp['VALUE']);
			 		elseif ($propCode == 'PRICE' || $propCode == 'PRICE_DISCOUNT')
			 			$this->apartment['PROPS'][$propCode] = array(
					 		'NAME' => $arProp['NAME'],
					 		'VALUE' => number_format($arProp['VALUE'], 0, '.', ' ') . ' руб.'
					 	);
			 		else
					 	$this->apartment['PROPS'][$propCode] = array(
					 		'NAME' => $arProp['NAME'],
					 		'VALUE' => $arProp['VALUE']
					 	);
				}
			}
			$house_info = self::getHouseInfo($this->house_id);
			$this->apartment['MAP_HOUSE'] = $house_info['map'];
			$this->apartment['ADDRESS'] = $house_info['address'];
			return $this->apartment;
		}

		public function getPlan($name_plan)
		{
			if ($name_plan)
			{
				$cache = new CPHPCache();
				$cache_time = 86400;
				$cache_id = 'plan_' . $name_plan;
				$cache_path = 'plans';

				if ($cache_time > 0 && $cache->InitCache($cache_time, $cache_id, $cache_path))
				{
				   	$res = $cache->GetVars();
				   	if ($res["plan"])
						$plan = $res["plan"];
				}

				if (!$plan)
				{
					$arSelect = Array("NAME", "PREVIEW_PICTURE");
					$arFilter = Array("IBLOCK_ID"=>$this->iblock_id_plan, "NAME" => $name_plan);
					$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
					if ($ob = $res->GetNextElement())
					{
						$arFields = $ob->GetFields();
						$file = CFile::ResizeImageGet($arFields['PREVIEW_PICTURE'], array('width'=>700, 'height'=>500), BX_RESIZE_IMAGE_PROPORTIONAL, true);
						$plan = $file['src'];
					}

					if ($cache_time > 0)
				    {
				        $cache->StartDataCache($cache_time, $cache_id, $cache_path);
				        $cache->EndDataCache(array("plan"=>$plan));
				    }
				}				
			}
			return $plan;
		}

		public function getContacts()
		{
			$arFilter = Array('IBLOCK_ID'=>$this->iblock_id_contacts, 'GLOBAL_ACTIVE'=>'Y', 'PROPERTY'=>Array('SRC'=>'https://%'));
			$db_list = CIBlockSection::GetList(Array('SORT'=>'ASC'), $arFilter, false);
			while($ar_result = $db_list->GetNext())
			{
				$contacts[$ar_result['ID']]['NAME'] = $ar_result['NAME'];
			}

			$arSelect = Array("ID", "NAME", "IBLOCK_SECTION_ID", "PROPERTY_EMAIL", "PROPERTY_PHONE");
			$arFilter = Array("IBLOCK_ID"=>$this->iblock_id_contacts, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
			$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
			while($ob = $res->GetNextElement())
			{
			 $arFields = $ob->GetFields();
			 $contacts[$arFields['IBLOCK_SECTION_ID']]['ITEMS'][] = array(
			 	'ADDRESS' => $arFields['NAME'],
			 	'PHONE' => $arFields['PROPERTY_PHONE_VALUE'],
			 	'EMAIL' => $arFields['PROPERTY_EMAIL_VALUE'],
			 );
			}
			return $contacts;
		}

		public function getHouseInfo($house_number)
		{
			if (!$house_number) return false;
			// получаем карту дома
			$cache = new CPHPCache();
			$cache_time = 86400;
			$cache_id = 'map_house_' . $house_number;
			$cache_path = 'plans';

			if ($cache_time > 0 && $cache->InitCache($cache_time, $cache_id, $cache_path))
			{
			   	$res = $cache->GetVars();
			   	if ($res["map_house"])
					$map_house = $res["map_house"];
				if ($res["address"])
					$address = $res["address"];
			}

			if (!$map_house || !$address)
			{
				$arSelect = Array("NAME", "PREVIEW_PICTURE");
				$arFilter = Array("IBLOCK_ID"=>$this->iblock_id_map, "NAME" => $house_info['NUMBER']);

				$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
				if ($ob = $res->GetNextElement())
				{
					$arFields = $ob->GetFields();
					$file = CFile::ResizeImageGet($arFields['PREVIEW_PICTURE'], array('width'=>700, 'height'=>500), BX_RESIZE_IMAGE_PROPORTIONAL, true);
					$map_house = $file['src'];
					$address = $arFields['NAME'];
				}

				if ($cache_time > 0)
			    {
			        $cache->StartDataCache($cache_time, $cache_id, $cache_path);
			        $cache->EndDataCache(array("map_house"=>$map_house, "address" => $address));
			    }
			}
			$house_info = array(
				'map' => $map_house,
				'address' => $address
			);
			return $house_info;
		}
	}
?>