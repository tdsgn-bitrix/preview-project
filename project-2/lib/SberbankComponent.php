<?php

namespace App\Bundle\Name;

use CIBlockElement;
use CIBlockPropertyEnum;
use CIBlock;

/**
 * Class SberbankComponent
 *
 * @package App\Bundle\Name
 */
class SberbankComponent
{
    /**
     * Ссылка API банка.
     */
    const SBERBANK_API_URL = 'https://3dsec.sberbank.ru/payment/rest/';

    /**
     * Набор конфигураций.
     *
     * @var array
     */
    protected $config = [
        'token' => ''
    ];

    /**
     * Данные авторизации продавца.
     *
     * @var array
     */
    protected $merchantAccount = [
        'password' => '',
        'userName' => ''
    ];

    /**
     * Набор методов API банка.
     *
     * @var array
     */
    protected $method = [
        'reg' => 'registerPreAuth.do',
        'status' => 'getOrderStatusExtended.do',
        'close' => 'deposit.do'
    ];

    /**
     * Получить идентификатор инфоблока с оплатой.
     *
     * @return mixed
     */
    public function getInfoBlockId()
    {
        $block = CIBlock::GetList(Array(), Array('TYPE' => 'info_pay', 'SITE_ID' => SITE_ID, 'CODE' => 'pay'), false)->Fetch();
        return $block["ID"];
    }

    /**
     * Подготовка данных для инициализации платежа.
     */
    public function prepareData()
    {
        if (empty($_POST) || empty($_POST['NAME']) || empty($_POST['PAY']) || empty($_POST['PHONE']) || empty($_POST['amount'])) {
            //Ошибка пустой запрос.
            return header('Location: ' . $this->getReturnUrl('error'));
        } else {
            $fields = [
                "IBLOCK_ID" => $this->getInfoBlockId(),
                "NAME" => $_POST['NAME'] . ' ' . $_POST['SECOND_NAME'],
                "PROPERTY_VALUES" => [
                    'NAME' => $_POST['NAME'],
                    'SECOND_NAME' => $_POST['SECOND_NAME'],
                    'PHONE' => $_POST['PHONE'],
                    'PAY' => $_POST['PAY'],
                    'amount' => $this->getAmountPay($_POST['amount']),
                    'amountRub' => number_format($this->getAmountPay($_POST['amount'] / 100), 2, ',', ','),
                    'type_pay' => 'Сбербанк'
                ]
            ];
            $element = new CIBlockElement;
            if ($elementId = $element->Add($fields)) {
                $this->initPay($elementId);
            } else {
                //Ошибка не удалось создать запись платежа.
                return header('Location: ' . $this->getReturnUrl('error'));
            }
        }
    }

    /**
     * Получить сумму платежа в копейках.
     *
     * @param string $amountPay сумма в рублях.
     *
     * @return string
     */
    public function getAmountPay($amountPay)
    {
        $amountPay = (integer)round($amountPay * 100, 0);
        if (is_integer($amountPay)) {
            return $amountPay;
        } else {
            //Ошибка число должно быть целым или с плавающей точкой.
            return header('Location: ' . $this->getReturnUrl('error'));
        }
    }

    /**
     * Инициализация платежа.
     *
     * @param string $payId идентификатор записи платежа.
     */
    public function initPay($payId)
    {
        if (!empty($payObject = $this->getPayObject(['ID' => $payId]))) {

            $data = [
                'amount' => $payObject['arProps']['amount']["~VALUE"],
                'orderNumber' => $payObject['arFields']['ID'],
                'returnUrl' => $this->getReturnUrl('success'),
                'failUrl' => $this->getReturnUrl('error'),
                'description' => $this->getDescription($payObject)
            ];

            $request = $this->sendRequest($this->method['reg'], array_merge($data, $this->config));
            $resultRequest = json_decode($request);

            $this->setStatusPay($resultRequest->orderId);


            if (empty($resultRequest->formUrl) && !empty($resultRequest->errorMessage)) {
                echo $resultRequest->errorMessage;
                return;
            } else {
                if ($this->updatePayObject($payId, 'orderNumber', $resultRequest->orderId)) {
                    return header('Location: ' . $resultRequest->formUrl);
                } else {
                    //Ошибка при записи атрибута orderId в запись платежа.
                    return header('Location: ' . $this->getReturnUrl('error'));
                }
            }
        } else {
            //Ошибка при получении записи платежа.
            return header('Location: ' . $this->getReturnUrl('error'));
        }
    }

    /**
     * Подготовка данных для подтверждения оплаты в банке.
     */
    public function success()
    {
        if (empty($_GET['orderId'])) {
            //Ошибка при получении номера платежа.
            return header('Location: ' . $this->getReturnUrl('error'));
        } else {
            if ($this->getStatusPay($_GET['orderId']) === (integer)1) {
                $this->closePay($_GET['orderId']);
            } else {
                //Ошибка - не корректный статус платежа.
                return header('Location: ' . $this->getReturnUrl('error'));
            }
        }
    }

    /**
     * Получить статус платежа.
     *
     * @param string $orderId идентификатор платежа.
     *
     * @return mixed
     */
    public function getStatusPay($orderId)
    {
        $data = array_merge(['orderId' => $orderId], $this->config);
        $request = $this->sendRequest($this->method['status'], $data);
        $resultRequest = json_decode($request);
        return $resultRequest->orderStatus;
    }

    /**
     * Установить статус платежа для записи.
     *
     * @param string $orderId идентификатор записи платежа.
     * @param string $column column.
     */
    public function setStatusPay($orderId, $column = 'orderNumber')
    {
        $property_enums = CIBlockPropertyEnum::GetList([], Array("IBLOCK_ID" => $this->getInfoBlockId(), "CODE" => "status"));
        $status = [];
        while ($enum_fields = $property_enums->GetNext()) {
            $status [$enum_fields["XML_ID"]] = $enum_fields["ID"];
        }
        $payObject = $this->getPayObject([$column => (string)$orderId]);
        $payStatusId = $this->getStatusPay($orderId);
        CIBlockElement::SetPropertyValues($payObject['arFields']['ID'], $this->getInfoBlockId(),
            $status[$payStatusId + 1], 'status');


        AddMessage2Log("Статус сбер: " . $payStatusId . " Расшифровка: " . $status[$payStatusId + 1] . " Номер заказа: " . $orderId);
    }

    /**
     * Отправить подтверждение оплаты в банк.
     *
     * @param string $orderId идентификатор сделки в сбербанке.
     * @param string $column column.
     */
    public function closePay($orderId, $column = 'orderNumber')
    {
        $data = array_merge(['amount' => '0', 'orderId' => $orderId], $this->merchantAccount);
        $this->sendRequest($this->method['close'], $data);

        AddMessage2Log("Запрос на закрытмие платежа отправлен");

        $this->setStatusPay($orderId, $column);
    }

    /**
     * Указать значение свойства элемента инфоблока.
     *
     * @param string $payId идентификатор записи.
     * @param string $code код свойства.
     * @param string $value значение свойства.
     *
     * @return bool
     */
    public function updatePayObject($payId, $code, $value)
    {
        return CIBlockElement::SetPropertyValueCode($payId, $code, $value);
    }

    /**
     * Отправить запрос.
     *
     * @param string $method method.
     * @param array $data data.
     *
     * @return bool|string
     */
    public function sendRequest($method, $data)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::SBERBANK_API_URL . $method);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        $result = curl_exec($ch);
        curl_close($ch);
        AddMessage2Log("Метод: " . $method . " Данные: " . json_encode($data));
        return $result;
    }

    /**
     * Получить модель платежа по идентификатору.
     *
     * @param array $filter условия выборки.
     *
     * @return array
     */
    public function getPayObject($filter)
    {
        $arSelect = Array("IBLOCK_ID", "ID", "NAME", "PROPERTY_*");
        $arFilter = Array("IBLOCK_ID" => $this->getInfoBlockId(), $filter);
        $infoBlock = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
        $payObject = [];
        while ($object = $infoBlock->GetNextElement()) {
            $payObject = [
                'arFields' => $object->GetFields(),
                'arProps' => $object->GetProperties()
            ];
        }
        return $payObject;
    }

    /**
     * Получить описание платежа.
     *
     * @param array $payObject модель платежа.
     *
     * @return string
     */
    public function getDescription($payObject)
    {
        $description =  $payObject['arProps']['PAY']['~VALUE'];
        $description .= ' Имя заказчика: ' . $payObject['arFields']['NAME'];
        $description .= ' Номер телефона: ' . $payObject['arProps']['PHONE']['~VALUE'];
        return $description;

    }

    /**
     * Получить ссылку на страницу результата оплаты.
     *
     * @param string $template шаблон.
     *
     * @return string
     */
    public function getReturnUrl($template)
    {
        return $_SERVER['HTTP_ORIGIN'] . DIRECTORY_SEPARATOR . 'pay/' . $template . '.php';
    }

    /**
     * Обработка callback нотификаций от банка.
     */
    public function callback()
    {
        $requestData = $_GET;
        $checksum = $requestData['checksum'];
        unset($requestData['checksum']);
        ksort($requestData);
        $requestDataString = '';

        foreach ($requestData as $key => $value) {
            $requestDataString .= $key . ';' . $value . ';';
        }

        $hmac = hash_hmac('sha256', $requestDataString, $this->config['token']);

        if (!empty($requestData['mdOrder']) && $hmac === $checksum) {
            $this->closePay($requestData['mdOrder']);
        }
    }
}
