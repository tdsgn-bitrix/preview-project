<?php

namespace App\Bundle\Name;

use CSaleOrder;
use DateInterval;
use DateTime;

class SmsComponent
{
    /**
     * Ссылка API.
     */
    const API_URL = 'http://api.inform.pimpay.ru/send-sms';

    /**
     * Ключ
     */
    const API_KEY = '';

    /*
     * Формирование запроса
     *
     * */
    public function getRequest($data)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::API_URL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type:application/json",
            "authorization:" . self::API_KEY,
        ));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    /*
     * Формтрование шаблона смс
     *
     * */
    public function getMessage($phone, $orderNumber, $orderPrice)
    {
        $date = new DateTime();
        $date->add(new DateInterval('P14D'));
        $day = $date->format('d.m') . "\n";

        $message = [
            'phoneNumber' => $phone,
            'text' => 'Заказ №' . $orderNumber . ' на ' . $orderPrice . 'р. создан, ожидайте ' . $day
        ];

        return $this->getRequest(json_encode($message));
    }

    /**
     * Отправить смс сообщение.
     * @param string $phone номер получателя.
     * @param string $text текст сообщения.
     * @return bool|string
     */
    public function sendSms($phone, $text)
    {
        return $this->getRequest(json_encode(['phoneNumber' => $phone, 'text' => $text]));
    }
}