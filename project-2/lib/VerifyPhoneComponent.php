<?php

namespace App\Bundle\Name;

use App\Bundle\LandingPage\SettingInfoBlock;
use CIBlockElement;
use CIBlockSection;
use CIBlockPropertyEnum;
use CIBlock;
use DateInterval;
use DateTime;
use Exception;


/**
 * Class VerifyPhoneComponent
 * @package App\Bundle\Name
 */
class VerifyPhoneComponent
{
    /**
     * Продолжительность актуальности смс кода, в минутах.
     * @var int
     */
    public $timeOutMinutes = 5;

    /**
     * Колличетво минут, на которое будет заблокирована отправка смс кода, одному пользователю,
     * при привышении лимита в $limitSmsCount смс.
     * @var int
     */
    public $timeOutSendSms = 60;

    /**
     * Колличество смс, которые могут быть отправлены одному пользователю за $timeOutSendSms минут.
     * @var int
     */
    public $limitSmsCount = 3;

    /**
     * Отправить смс код подтверждения.
     * @param string $phone номер телефона.
     * @return bool|string
     */
    public function sendSmsCode($phone)
    {
        $messages = $this->test($phone);
        if (count($messages) >= $this->limitSmsCount) {
            $dateTime = date('Y-m-d H:i:s', current($messages)["arProps"]["DATE"]["VALUE"]);

            $interval = new DateInterval('PT' . $this->timeOutSendSms . 'M');
            $intervalDate = (new DateTime($dateTime))->add($interval)->format('Y-m-d H:i:s');

            $currentDate = (new DateTime())->format('Y-m-d H:i:s');

            $difference = strtotime($intervalDate) - strtotime($currentDate);
            $timeout = $difference > 0 ? $difference : null;

            $errorMessage = (new SettingInfoBlock())->getSetting('SMS_TIMEOUT', false)["~VALUE"];
            $result = ['status' => 'error', 'error' => $errorMessage, 'timer' => $timeout];

            return json_encode($result);
        }

        $element = new CIBlockElement;
        $code = $this->generateRandomString(4, true);
        $fields = [
            "IBLOCK_ID" => $this->getInfoBlockId(),
            "NAME" => 'sms',
            "PROPERTY_VALUES" => [
                'PHONE' => $phone,
                'DATE' => (new DateTime())->getTimestamp(),
                'CODE' => $code,
                'KEY' => $this->generateRandomString(),
            ]
        ];


        $result = (new SmsComponent())->sendSms($phone, $code);
        $resultObject = json_decode($result);

        if ($resultObject->status == 'error')
            return json_encode(['status' => 'error',
                'error' => (new SettingInfoBlock())->getSetting('SMS_ERROR', false)["~VALUE"]]);

        $element->Add($fields);
        return $result;
    }

    public function test($phone)
    {
        $interval = new DateInterval('PT' . $this->timeOutSendSms . 'M');
        $timeFilter = (new DateTime())->sub($interval)->getTimestamp();
        $arSelect = array("IBLOCK_ID", "ID", "NAME", "PHONE", "DATE", "PROPERTY_*");
        $arFilter = array("IBLOCK_ID" => $this->getInfoBlockId(),
            'PROPERTY_PHONE' => $phone, '>PROPERTY_DATE' => $timeFilter);
        $infoBlock = CIBlockElement::GetList(['ID' => SORT_DESC], $arFilter, false, array(), $arSelect);
        $payObject = [];
        while ($object = $infoBlock->GetNextElement()) {
            $payObject[] = [
                'arFields' => $object->GetFields(),
                'arProps' => $object->GetProperties()
            ];
        }
        return $payObject;
    }

    /**
     * Получить идентификатор инфоблока.
     * @return mixed
     */
    public function getInfoBlockId()
    {
        $block = CIBlock::GetList([], ['TYPE' => 'sms', 'SITE_ID' => SITE_ID, 'CODE' => 'sms'], false)->Fetch();
        return $block["ID"];
    }

    /**
     * Получить ключ.
     * @param int $length длина ключа.
     * @param false $int только цифры.
     * @return false|string
     */
    public function generateRandomString($length = 16, $int = false)
    {
        $symbol = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        if ($int) $symbol = '0123456789';
        return substr(str_shuffle(str_repeat($x = $symbol, ceil($length / strlen($x)))), 1, $length);
    }

    /**
     * Проверить смс код подтверждения.
     * @param string $phone номер телефона.
     * @param string $code код.
     * @return false|string
     * @throws Exception
     */
    public function checkSmsCode($phone, $code)
    {
        $object = $this->getSmsObject($phone, $code);
        $objectAfterTime = $this->getSmsObject($phone, $code, true);
        if (!empty($object)) {
            CIBlockElement::SetPropertyValues($object['arFields']['ID'], $this->getInfoBlockId(), 'Y', 'STATUS');
            return json_encode(['status' => 'success']);
        } elseif (empty($object) && !empty($objectAfterTime)) {
            return json_encode(['status' => 'timeout', 'timeout' => false,
                'error' => (new SettingInfoBlock())
                    ->getSetting('SMS_TIME_ERROR', false)["~VALUE"]]);
        }
        return json_encode(['status' => 'error', 'timeout' => true,
            'error' => (new SettingInfoBlock())
                ->getSetting('SMS_CHECK_ERROR', false)["~VALUE"]]);
    }

    /**
     * Получить объект кода подтверждения.
     * @param string $phone номер телефона.
     * @param string $code код.
     * @param false $afterTime искать утаревший код.
     * @return array
     * @throws Exception
     */
    public function getSmsObject($phone, $code, $afterTime = false)
    {
        $interval = new DateInterval('PT' . $this->timeOutMinutes . 'M');
        $timeFilter = (new DateTime())->sub($interval)->getTimestamp();
        $arSelect = array("IBLOCK_ID", "ID", "NAME", "PHONE", "DATE", "PROPERTY_*");
        $arFilter = array("IBLOCK_ID" => $this->getInfoBlockId(),
            'PROPERTY_PHONE' => $phone, 'PROPERTY_CODE' => $code, '>PROPERTY_DATE' => $timeFilter);
        if ($afterTime) {
            $arFilter = array("IBLOCK_ID" => $this->getInfoBlockId(), 'PROPERTY_CODE' => $code,
                'PROPERTY_PHONE' => $phone);
        }
        $infoBlock = CIBlockElement::GetList(['ID' => SORT_DESC], $arFilter, false, array(), $arSelect);
        $payObject = [];
        while ($object = $infoBlock->GetNextElement()) {
            $payObject = [
                'arFields' => $object->GetFields(),
                'arProps' => $object->GetProperties()
            ];
        }
        return $payObject;
    }

    /**
     * Получить статус проверки заказа, через смс код.
     *
     *
     * @param string $landingId идентификатор лендинга.
     * @param string $cart корзина пользователя.
     * @return bool
     */
    public function isActiveComponent($landingId, $cart)
    {
        $countries = (new SettingInfoBlock())->getSetting('SMS_COUNTRY', true);
        $section = \TAO::infoblock('landing_page')->getSectionByCode($landingId);
        $country = $section->getUserField('UF_COUNTRY');
        $result = false;

        if (in_array($country, $countries)) $result = true;
        if (!$section->getUserField('UF_VERIFY')) $result = false;

        $settingCart = $section->getUserField('UF_VERIFY_CART');
        if (!empty($settingCart)) {
            $cart = array_column(json_decode($cart, true), 'id');
            foreach ($cart as $item) {
                if (in_array($item, $settingCart)) {
                    $result = true;
                }
            }
        }
        return $result;
    }
}
