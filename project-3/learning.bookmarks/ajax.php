<?php
require_once $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php";
require_once "functions.php";

global $USER;
if ($USER->IsAuthorized() && isset($_REQUEST["action"]) && isset($_REQUEST["id"])) {
    $arUser = CUser::GetByID($USER->GetID())->Fetch();

    if (isset($arUser["UF_BOOKMARKS"]) && is_array($arUser["UF_BOOKMARKS"])) {
        $arBookmarks = $arUser["UF_BOOKMARKS"];
    } else {
        $arBookmarks = [];
    }

    $sBookmark = $_REQUEST["id"];
    if (!isBookmarkAllowed($sBookmark)) {
        return;
    }

    switch ($_REQUEST["action"]) {
        case "add":
            if (!in_array($sBookmark, $arBookmarks)) {
                array_push($arBookmarks, $sBookmark);
            }
            break;

        case "remove":
            if (in_array($sBookmark, $arBookmarks)) {
                $arBookmarks = array_diff($arBookmarks, [$sBookmark]);
            }
            break;

        default:
            return;
    }

    $obUser = new CUSer();
    $bUpdated = $obUser->Update($USER->GetID(), [
        "UF_BOOKMARKS" => $arBookmarks
    ]);

    if ($bUpdated) {
        echo "OK";
    }
}


require_once $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php";