<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="bookmark-list" style="padding:20px;">
    <h2 class="bookmark-list--title">Закладки</h2>
<?if (!empty($arResult["LESSONS"])):?>
    <table>
        <tr class="bookmark-list--header">
            <th>#</th>
            <th>Курс</th>
            <th>Урок</th>
            <th></th>
        </tr>
	<?foreach($arResult["LESSONS"] as $index => $arLesson):?>
        <tr class="bookmark-list--row">
            <td><?= $index + 1 ?></td>
            <td><?=$arLesson["COURSE_NAME"]?></td>
            <td>
                <a href="<?=$arLesson["URL"]?>" target="_blank"><?=$arLesson["NAME"]?></a>
            </td>
            <td>
                <a href="#" class="bookmark-delete" data-id="<?=$arLesson["LESSON_ID"]?>">Удалить</a>
            </td>
        </tr>
	<?endforeach;?>
    </table>
<?else:?>
    <p>Закладки пусты.</p>
<?endif?>
</div>
