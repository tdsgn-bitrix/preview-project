
$(document).ready(function() {
    $(".bookmark-delete").on("click", function(event) {
        event.preventDefault();

        $.ajax("/local/components/learning.bookmarks/ajax.php", {
            method: "POST",
            data: {
                action: "remove",
                id: $(this).data("id")
            }
        }).done(function (data) {
            if (data === "OK") {
                window.location.reload();
            } else {
                alert("При удалении закладки произошла ошибка.");
            }
        }).fail(function(xhr, error) {
            alert("При удалении закладки произошла ошибка.");
        });
    });
});
