<?php

require_once "functions.php";

class LearningBookmarks extends CBitrixComponent {
    public function executeComponent()
    {
        try {
            $this->prepareLessons();
            $this->prepareCourses();
        } catch (LearnException $obException) {
            echo "При получении закладок произошла ошибка.";
            $this->arResult = [];
        }
        
        $this->includeComponentTemplate();
    }

    private function prepareLessons()
    {
        global $USER;
        if (!CModule::IncludeModule('learning')) {
            return false;
        }

        $arUser = CUser::GetByID($USER->GetID())->Fetch();
        $arBookmarks = $arUser["UF_BOOKMARKS"];

        $this->arResult["LESSONS"] = [];
        if (empty($arBookmarks)) {
            return;
        }

        $arOrder = [
            "LESSON_ID" => "ASC"
        ];
        $arFilter = [
            "LESSON_ID" => $arBookmarks,
            "ACTIVE" => "Y",
            "CHECK_PERMISSIONS" => "N"
        ];
        $arSelectFields = [
            "LESSON_ID",
            "NAME"
        ];

        // Не-администратор может иметь права только на уроки верхнего уровня (точнее, GetList выбирает только их).
        $obLessons = CLearnLesson::GetList($arOrder, $arFilter, $arSelectFields);
        while ($arLesson = $obLessons->Fetch()) {
            // Поэтому при выборке закладок проверяем права на курсы отдельно.
            if (isBookmarkAllowed($arLesson["LESSON_ID"])) {
                array_push($this->arResult["LESSONS"], $arLesson);
            }
        }
    }

    private function prepareCourses()
    {
        foreach ($this->arResult["LESSONS"] as $nKey => $arLesson) {
            $sLessonId = $arLesson["LESSON_ID"];

            $obPath = CLearnLesson::GetListOfParentPathes($sLessonId)[0];
            $sLessonPath = $obPath->ExportUrlencoded() . "." . $sLessonId;

            $arCourse = CLearnLesson::GetByID($obPath->GetTop())->Fetch();

            $sCourseName = $arCourse["NAME"];
            $sCourseId = $arCourse["COURSE_ID"];

            $this->arResult["LESSONS"][$nKey]["COURSE_NAME"] = $sCourseName;
            $this->arResult["LESSONS"][$nKey]["URL"] = "/courses/course$sCourseId/lesson$sLessonId/?LESSON_PATH=$sLessonPath";
        }
    }
}
