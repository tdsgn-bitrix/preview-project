<?php

function isBookmarkAllowed($sLessonID) {
    if (!CModule::IncludeModule('learning')) {
        return false;
    }

    // Определеяем ID урока верхнего уровня, к которому относится данный урок.
    $arPaths = CLearnLesson::GetListOfParentPathes($sLessonID);
    if (empty($arPaths)) {
        return false;
    }
    $nTopID = $arPaths[0]->GetTop();

    // Ищем ID урока верхнего уровня среди доступных пользователю.
    return (CLearnLesson::GetByID($nTopID)->Fetch() !== false);
}
