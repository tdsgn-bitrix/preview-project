<?php

class LearningCourseActions extends CBitrixComponent {
    public function executeComponent()
    {
        global $USER;
        if (!CModule::IncludeModule('learning')) {
            ShowError("Модуль обучения не установлен");
            return false;
        }

        $attemptId = intval($this->arParams['ATTEMPT_ID']);
        $arAttempt = CTestAttempt::GetList(
            ["ID" => "DESC"],
            [
                "ID" => $attemptId,
                "STATUS" => "F",
                "STUDENT_ID" => $USER->GetID(),
            ]
        )->Fetch();

        $this->arResult['ATTEMPT'] = $arAttempt;
        $this->arResult['TEST'] = CTest::GetByID($arAttempt['TEST_ID'])->Fetch();
        $this->arResult['COURSE'] = CCourse::GetByID($arAttempt["COURSE_ID"])->Fetch();
        if ($arAttempt) {
            $rsUserAnswers = CTestResult::GetList(
                ["ID" => "DESC"],
                [
                    "ATTEMPT_ID" => $arAttempt["ID"],
                ]
            );

            $answersIds = [];
            $arUserAnswers = [];
            while ($arUserAnswer = $rsUserAnswers->Fetch()) {
                $arUserAnswers[$arUserAnswer["QUESTION_ID"]] = $arUserAnswer;
                $questionsIds[] = $arUserAnswer['QUESTION_ID'];
            }

            $arQuestions = [];
            $rsQuestions = CLQuestion::GetList(['SORT' => 'ASC'], ['ID' => $questionsIds]);
            while($arQuestion = $rsQuestions->Fetch()) {
                $arQuestion["ANSWERS"] = $this->getQuestionAnswers($arQuestion["ID"], false);
                if ($arQuestion['QUESTION_TYPE'] == "R") {
                    $arQuestion["CORRECT_ANSWER"] = array_keys($arQuestion["ANSWERS"]);
                } else {
                    $arQuestion["CORRECT_ANSWER"] = $this->getCorrectAnswers($arQuestion["ANSWERS"]);
                }
                $arQuestions[$arQuestion["ID"]] = $arQuestion;
            }
            $this->arResult['QUESTIONS'] = $arQuestions;

            $errors = 0;
            foreach ($arUserAnswers as &$userAnswer) {
                $question = $arQuestions[$userAnswer["QUESTION_ID"]];
                $response = explode(',', $userAnswer["RESPONSE"]);

                if ($question["QUESTION_TYPE"] == "R") {
                    $correct = $question["CORRECT_ANSWER"] == $response;
                } else {
                    $t1 = array_diff($question["CORRECT_ANSWER"],$response);
                    $t2 = array_diff($response,$question["CORRECT_ANSWER"]);
                    $correct = $response && ($t1 == $t2 && $t2 == Array());
                }
                if (!$correct) {
                    $errors++;
                }
                $userAnswer["CORRECT"] = $correct ? "Y" : "N";
            }

            $this->arResult["ATTEMPT"]["ERRORS"] = $errors;
            $this->arResult['ANSWERS'] = $arUserAnswers;
            $this->includeComponentTemplate();
        } else {
            ShowError("Попытка не найдена");
        }
    }

    public function getQuestionAnswers($questionId, $correctOnly = true) {
        $answers = [];
        $arFilter = [
            'QUESTION_ID' => $questionId,
        ];
        if ($correctOnly) {
            $arFilter["CORRECT"] = "Y";
        }
        $rs = CLAnswer::GetList(
            [ 'SORT' => 'ASC', ],
            $arFilter
        );
        while($ar = $rs->Fetch()) {
            $answers[$ar["ID"]] = $ar;
        }
        return $answers;
    }

    private function getCorrectAnswers($answers) {
        $correctAnswers = [];
        foreach ($answers as $answer){
            if ($answer["CORRECT"] == "Y")
                $correctAnswers[] = $answer["ID"];
        }

        return $correctAnswers;
    }
}