<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="attempt-answers">
    <h2 class="attempt-answers--title">Отчет по тестированию "<?=$arResult["COURSE"]["NAME"]?>"</h2>
    <table>
        <tr class="attempt-answers--header">
            <th class="col-question">Вопрос</th>
            <th class="col-answer">Ваш ответ</th>
            <th class="col-result">Результат</th>
        </tr>
        <? foreach($arResult['ANSWERS'] as $arAnswer):
            $arQuestion = $arResult["QUESTIONS"][$arAnswer["QUESTION_ID"]];
            $response = explode(',', $arAnswer["RESPONSE"]);
            ?>
        <tr class="attempt-answers--row">
            <td><?= $arQuestion['NAME'] ?></td>
            <td>
                <? if($arAnswer['CORRECT'] == "Y"): ?>
                    <div class="attempt-answers--correct-answer">
                        <? foreach ($response as $answerId): ?>
                            <?= $arQuestion["ANSWERS"][$answerId]["ANSWER"] ?><br>
                        <? endforeach; ?>
                    </div>
                <? else: ?>
                    <div class="attempt-answers--wrong-answer">
                        <? foreach ($response as $answerId): ?>
                            <?= $arQuestion["ANSWERS"][$answerId]["ANSWER"] ?><br>
                        <? endforeach; ?>
                    </div>
                    <br>
                    Правильный ответ:
                    <div class="attempt-answers--correct-answer">
                        <? foreach ($arQuestion["CORRECT_ANSWER"] as $answerId): ?>
                            <?= $arQuestion["ANSWERS"][$answerId]["ANSWER"] ?><br>
                        <? endforeach; ?>
                    </div>
                <? endif; ?>
            </td>
            <td class="col-result">
                <? if($arAnswer['CORRECT'] == "Y"): ?>
                    <span class="icon icon--correct"></span>
                <? else: ?>
                    <span class="icon icon--error"></span>
                <? endif; ?>
            </td>
        </tr>
        <? endforeach; ?>
    </table>
    <div class="attempt-details">
        <dl>
            <dt>Дата:</dt>
            <dt><?= $arResult["ATTEMPT"]["DATE_START"] ?></dt>
        </dl>
        <dl>
            <dt>Ошибок:</dt>
            <dt><?= $arResult["ATTEMPT"]["ERRORS"] ?></dt>
        </dl>
        <dl>
            <dt>Набрано баллов:</dt>
            <dt><?= $arResult["ATTEMPT"]["SCORE"] ?> / <?= $arResult["ATTEMPT"]["MAX_SCORE"] ?></dt>
        </dl>
        <dl>
            <dt>Необходимо для завершения:</dt>
            <dt><?= ceil($arResult["ATTEMPT"]["MAX_SCORE"] * ($arResult["TEST"]["COMPLETED_SCORE"] / 100)) ?></dt>
        </dl>
    </div>
</div>