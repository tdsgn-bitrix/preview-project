<?php

class LearningCourseActions extends CBitrixComponent {
    public function onPrepareComponentParams($arParams)
    {
        $arParams["CHECK_PERMISSIONS"] = (isset($arParams["CHECK_PERMISSIONS"]) && $arParams["CHECK_PERMISSIONS"]=="N" ? "N" : "Y");
        return $arParams;
    }

    public function executeComponent()
    {
        global $USER;
        if (!CModule::IncludeModule('learning')) {
            return false;
        }

        if ($this->request->getQuery('action') == 'lesson.complete' && check_bitrix_sessid() && $this->checkPermission()) {
            $courseId = $this->arParams['COURSE_ID'];
            $lessonId = $this->arParams['LESSON_ID'];

            if ($courseId && $lessonId) {
                LessonCompletionTable::completeLesson($USER->GetID(), $lessonId);
            }
        }

        $nextContentId = intval($this->request->getQuery('next'));
        if ($nextContentId) {
            $arContent = $this->getContents($nextContentId);

            if ($arContent["TYPE"] == "CH") {
                $itemURL = CComponentEngine::MakePathFromTemplate($this->arParams["CHAPTER_DETAIL_TEMPLATE"],
                    Array("CHAPTER_ID" => '0' . $arContent["ID"], "COURSE_ID" => $this->arParams["COURSE_ID"])
                );
            } else {
                $itemURL = CComponentEngine::MakePathFromTemplate($this->arParams["LESSON_DETAIL_TEMPLATE"],
                    array(
                        "LESSON_ID" => $arContent["ID"],
                        "COURSE_ID" => $this->arParams["COURSE_ID"]
                    )
                );
            }

            if ($itemURL) {
                LocalRedirect($itemURL);
            }
        }
        return true;
    }

    private function getContents($nextChapter)
    {
        $arContents = CLearnCacheOfLessonTreeComponent::GetData($this->arParams['COURSE_ID']);

        foreach ($arContents as $arContent) {
            if ($arContent['ID'] == $nextChapter) {
                return $arContent;
            }
        }
        return false;
    }

    private function checkPermission()
    {
        $isAccessible = true;
        if ($this->arParams['CHECK_PERMISSIONS'] !== "N") {
            try {
                $arPermissionsParams = array(
                    'COURSE_ID' => $this->arParams['COURSE_ID'],
                    'LESSON_ID' => $this->arParams['LESSON_ID']
                );

                $isAccessible = CLearnAccessMacroses::CanUserViewLessonAsPublic($arPermissionsParams);
            } catch (Exception $e) {
                $isAccessible = false;    // access denied
            }

            if (!$isAccessible) {
                ShowError(GetMessage('LEARNING_COURSE_DENIED'));
            }
        }
        return $isAccessible;
    }
}