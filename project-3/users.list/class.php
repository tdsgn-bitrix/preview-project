<?php

class UsersList extends CBitrixComponent {

    public function executeComponent()
    {
        global $USER;

        if (!CModule::IncludeModule('learning')) {
            ShowError("Модуль 'learning' не подключен");
            return false;
        }
        $this->arResult["USER_CAN_EDIT"] = $USER->CanDoOperation('edit_all_users');
        $cacheTime = false; // false - use cache time from arParams, 0 - disable cache

        $arFilter = [
            "GROUPS_ID" => array_keys($this->getGroups($this->arParams['GROUPS_ID']))
        ];
        if ($_REQUEST['f'] && !$_REQUEST['f']['del']) {
            $filterFields = [];
        
            if (trim($_REQUEST['f']['WORK_COMPANY'])) {
                $work_company = (htmlspecialcharsbx($_REQUEST['f']['WORK_COMPANY']));
                $filterFields['WORK_COMPANY'] = $work_company;
                $arFilter['WORK_COMPANY'] = $work_company;
            }
            if (trim($_REQUEST['f']['LAST_NAME'])) {
                $last_name = (htmlspecialcharsbx($_REQUEST['f']['LAST_NAME']));
                $filterFields['LAST_NAME'] = $last_name;
                $arFilter['LAST_NAME'] = $last_name;
            }
            if (trim($_REQUEST['f']['GROUP_ID'])) {
                $group_id = (intval($_REQUEST['f']['GROUP_ID']));
                $filterFields['GROUP_ID'] = $group_id;
                $arFilter['GROUPS_ID'] = $group_id;

                $course = $this->getCourseByGroupID($group_id);
                $this->arResult['SELECTED_COURSE'] = $course['ID'];

                if (!$course) {
                    $this->arResult['MESSAGE'] = 'Курс не найден для выбранной группы';
                }
                if ($_REQUEST['f']['EXAM_STATUS']) {
                    $filterFields['EXAM_STATUS'] = $_REQUEST['f']['EXAM_STATUS'];
                    $users_ids = [];
                    if ($course) {
                        $rs = CCertification::GetList([], ["COURSE_ID" => $course['ID']]);
                        while ($ar = $rs->Fetch()) {
                            $users_ids[] = $ar["STUDENT_ID"];
                        }
                        if ($users_ids) {
                            if ($_REQUEST['f']['EXAM_STATUS'] == 'in_process') {
                                $arFilter["ID"] = '~' . implode('& ~', $users_ids);
                            } else {
                                $arFilter["ID"] = implode('|', $users_ids);
                            }
                        } elseif ($_REQUEST['f']['EXAM_STATUS'] == 'passed') {
                            $arFilter["ID"] = -1; // чтобы не нашлось ни одного пользователя
                        }
                    }
                }
            }
            if (count($filterFields) > 0) {
                $cacheTime = 0;
            }
            $this->arResult['filter'] = $filterFields;
        }

        if ($this->startResultCache($cacheTime, $this->getCacheID())) {
            $arUsers = [];

            $this->arResult['listenersGroups'] = $this->getListenersGroups();

            $rsUsers = CUser::GetList(
                $by = "ID",
                $order = "ASC",
                $arFilter,
                array(
                    'SELECT' => [
                        "UF_DOCUMENTS_DONE",
                    ]
                )
            );
            $rsUsers->NavStart(50, true);
            while($arUser = $rsUsers->Fetch()) {

                if ($this->arResult["USER_CAN_EDIT"]) {
                    $arUser["EDIT_URL"] = "/bitrix/admin/user_edit.php?lang=ru&ID=" . $arUser["ID"];
                }
                $arUser["CERTIFICATIONS"] = [];
                $arUsers[$arUser["ID"]] = $arUser;
            }

            if (count($arUsers)) {
                $rs = CCertification::GetList([], ["STUDENT_ID" => array_keys($arUsers)]);
                while ($ar = $rs->Fetch()) {
                    $arUsers[$ar["USER_ID"]]["CERTIFICATIONS"][] = $ar;
                }
            }
            $this->arResult["USERS"] = $arUsers;
            $this->arResult["NAV_STRING"] = $rsUsers->GetNavPrint('Пользователи');
            
            $this->includeComponentTemplate();
        }
    }

    private function getGroups($GROUPS_ID)
    {
        $groups = [];
        $rs = CGroup::GetList($by="ID", $order="ASC",
            array(
                "STRING_ID" => implode('|', $GROUPS_ID),
                "STRING_ID_EXACT_MATCH" => "N"
            ));
        while($ar = $rs->Fetch()) {
            $groups[$ar["ID"]] = $ar;
        }
        return $groups;
    }

    private function getListenersGroups()
    {
        static $listenersGroups = [];
        if (!$listenersGroups) {
            $listenersGroups = $this->getGroups(['course_']);
        }
        return $listenersGroups;
    }

    private function getCourseByGroupID($group_id)
    {
        $groups = $this->getListenersGroups();
        $group = $groups[$group_id];
        $course = CCourse::GetList([], ['CODE' => $group['STRING_ID']])->Fetch();
        if (!$course) {
            $course = CCourse::GetList([], [
                'NAME' => [
                    'LOGIC' => 'OR',
                    trim($group['NAME']),
                    trim($group['DESCRIPTION'])
                ],
            ])->Fetch();
        }

        return $course;
    }
}