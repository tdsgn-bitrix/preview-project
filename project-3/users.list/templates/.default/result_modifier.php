<?php
$arResult['colors'] = [
    '#eae514',
    '#0465a8',
    '#21197b',
    '#5fa395',
    '#54fbf2',
    '#c94a4b',
    '#b022c0',
    '#d73778',
    '#6d923d',
    '#0c89fd',
    '#065e5b',
];

$arResult["colors_css"] = ".sq {
    width: 10px;
    height: 10px;
    float: left;
    margin: 5px;
    background: #eae514;
    border:1px solid #919191;}";
$arResult["colors_class"] = [];
foreach ($arResult['colors'] as $color) {
    $name = 'sq-' . substr($color, 1);
    $arResult['colors_css'] .= '.' . $name . '{ background-color: ' . $color . '}' . PHP_EOL;
    $arResult['colors_class'][] = 'sq ' . $name;
}