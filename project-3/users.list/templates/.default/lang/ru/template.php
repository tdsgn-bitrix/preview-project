<?php

$MESS["FIELD_WORK_COMPANY"] = "Компания";
$MESS["FIELD_EMAIL"] = "Email";
$MESS["FIELD_NAME"] = "Имя";
$MESS["FIELD_LAST_NAME"] = "Фамилия";
$MESS["FIELD_SECOND_NAME"] = "Отчество";
$MESS["FIELD_LAST_ACTIVITY_DATE"] = "Последняя активность";
$MESS["FIELD_LAST_LOGIN"] = "Последняя авторизация";
$MESS["FIELD_FINISHED_CERTIFICATES"] = "Пройденные сертификации";
$MESS["FIELD_UF_DOCUMENTS_DONE"] = "Документы сделаны";
$MESS["FIELD_VALUE_BOOL_YES"] = "да";
$MESS["FIELD_VALUE_BOOL_NO"] = "";
