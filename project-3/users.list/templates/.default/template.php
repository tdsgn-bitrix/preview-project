<?if($_REQUEST["msg"]):?>
<p style="padding:20px;" class="bg-success"><?=htmlspecialcharsbx($_REQUEST["msg"])?></p>
<?endif;?>

<?if($arResult["MESSAGE"]):?>
<p style="padding:20px;" class="bg-warning"><?=htmlspecialcharsbx($arResult["MESSAGE"])?></p>
<?endif;?>
<div class="row">
    <div class="col-xs-12">
        <?=$arResult["NAV_STRING"]?>
    </div>
</div>
<style>
    <?=$arResult['colors_css']?>
</style>

<div class="row">
    <div class="col-xs-12">
<form action="<?= $APPLICATION->GetCurPageParam('', ['del', 'filter']) ?>" class="form--filter">
    <div class="row">
        <div class="form-group col-xs-12 col-sm-4">
            <label for="filter_company">Компания</label>
            <input type="text" name="f[WORK_COMPANY]" value="<?=$arResult['filter']['WORK_COMPANY']?>" class="form-control" id="filter_company" placeholder="">
        </div>
        <div class="form-group col-xs-12 col-sm-4">
            <label for="filter_last_name">Фамилия</label>
            <input type="text" name="f[LAST_NAME]" value="<?=$arResult['filter']['LAST_NAME']?>" class="form-control" id="filter_last_name" placeholder="">
        </div>
    </div>
    <div class="row">
        <div class="form-group col-xs-12 col-sm-4">
<!--    GROUP_ID    -->
            <label for="filter_group_id">Программа</label>
            <select name="f[GROUP_ID]" id="filter_group_id" class="form-control" onchange="checkStatusField(this, '[name=&quot;f[EXAM_STATUS]&quot;]')">
                <option value=""></option>
                <? foreach ($arResult['listenersGroups'] as $group): ?>
                    <option value="<?= $group['ID'] ?>" <?=($arResult['filter']['GROUP_ID'] == $group["ID"] ? 'selected' : '')?>><?= $group['NAME'] ?></option>
                <? endforeach ?>
            </select>
        </div>
        <div class="form-group col-xs-12 col-sm-4">
            <label for="filter_exam_status">Статус экзамена</label>
            <select name="f[EXAM_STATUS]" id="filter_exam_status" class="form-control" <?= (!$arResult['filter']['GROUP_ID'] ? 'disabled' : '') ?>>
                <option value="" <?=($arResult['filter']['EXAM_STATUS'] == '' ? 'selected' : '')?>>Все</option>
                <option value="passed" <?=($arResult['filter']['EXAM_STATUS'] == 'passed' ? 'selected' : '')?>>Сдал</option>
                <option value="in_process" <?=($arResult['filter']['EXAM_STATUS'] == 'in_process' ? 'selected' : '')?>>Не сдал</option>
            </select>
        </div>
        <div class="col-xs-12">
            <button type="submit" class="btn btn-primary">поиск</button>
            <button type="submit" name="f[del]" value="1" class="btn btn-default">сбросить</button>
        </div>
    </div>
</form>
    </div>

    <div class="col-xs-12">
<table class="table table-bordered table-striped table-hover">
    <tr>
        <?foreach ($arParams["FIELDS"] as $fieldName):?>
            <th><?=GetMessage("FIELD_" . $fieldName)?></th>
        <?endforeach;?>
        <?if($arResult["USER_CAN_EDIT"]):?>
        <th></th>
        <?endif;?>
    </tr>
    <?foreach ($arResult["USERS"] as $arUser):?>
    <tr>
        <?foreach ($arParams["FIELDS"] as $fieldName):?>
            <td>
                <?if($fieldName == "FINISHED_CERTIFICATES"):
                    foreach($arUser["CERTIFICATIONS"] as $arCertificate):
                        $courseClass = $arResult['colors_class'][array_rand($arResult['colors_class'])];
                        if ($arResult['SELECTED_COURSE'] && $arResult['SELECTED_COURSE'] == $arCertificate['COURSE_ID']) {
                            $courseClass .= ' course--selected';
                        }
                        $certTitle = $arCertificate["COURSE_NAME"] . "\n";
                        $certTitle .= "Последнее обновление: " . $arCertificate["TIMESTAMP_X"] . "\n";
                        $certTitle .= "Баллов/Макс. баллов: " . $arCertificate["SUMMARY"] . "/" . $arCertificate["MAX_SUMMARY"];
                        ?>
                        <div title="<?= htmlspecialcharsbx($certTitle)?>" class="<?=$courseClass?>"></div>
                    <? endforeach;?>
                <?elseif($fieldName == "UF_DOCUMENTS_DONE"):?>
                    <?= $arUser[$fieldName] ? GetMessage('FIELD_VALUE_BOOL_YES') : GetMessage('FIELD_VALUE_BOOL_NO')?>
                <?else:?>
                    <?= $arUser[$fieldName] ?>
                <?endif;?>
            </td>
        <?endforeach;?>
        <?if($arResult["USER_CAN_EDIT"]):?>
            <td><a href="<?= $arUser["EDIT_URL"] ?>">редактировать</a></td>
        <?endif;?>
    </tr>
    <?endforeach?>
</table>
    </div>
</div>

<script>
    function checkStatusField(source, target) {
        $(target).attr('disabled', !$(source).val());
    }
</script>
<?=$arResult["NAV_STRING"]?>

