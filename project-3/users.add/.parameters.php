<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arUserFields = [
    "LAST_NAME" => "Фамилия",
    "NAME" => "Имя",
    "SECOND_NAME" => "Отчество",
    "EMAIL" => "Email",
    "LOGIN" => "Логин",
    "WORK_COMPANY" => "Компания",
    "PASSWORD" => "Пароль",
    "PASSWORD_CONFIRM" => "Подтверждение пароля",
    "SEND_NOTIFICATION" => "Оповестить слушателя",
    "GROUP_ID" => "Группы",
];
$arUserGroups = [];
$rs = CGroup::GetList($by="SORT", $order="ASC", array("ACTIVE" => "Y"));
while($ar = $rs->Fetch())
    $arUserGroups[$ar["ID"]] = "[{$ar["ID"]}] " . $ar["NAME"];
    
$arComponentParameters = array(
    "GROUPS" => array(
//        "SLIDE_SETTINGS" => array("NAME" => GetMessage("ADV_SLIDE_SETTINGS"), "SORT" => "150"),
//        "NAV_SETTINGS" => array("NAME" => GetMessage("ADV_NAV_SETTINGS"), "SORT" => "250")
    ),
    "PARAMETERS" => array(
        "REQUIRED" => Array(
            "NAME" => "Обязательные поля",
            "PARENT" => "BASE",
            "TYPE" => "LIST",
            "MULTIPLE" => "Y",
            "VALUES" => $arUserFields,
            "ADDITIONAL_VALUES" => "Y",
            "DEFAULT" => array(
                'EMAIL',
                'LOGIN',
                'PASSWORD',
                'PASSWORD_CONFIRM',
            ),
        ),
        "SHOW_GROUPS" => array(
            "NAME" => "Показывать группы \n(можно указывать код группы)",
            "PARENT" => "BASE",
            "TYPE" => "LIST",
            "MULTIPLE" => "Y",
            "VALUES" => $arUserGroups,
            "ADDITIONAL_VALUES" => "Y",
            "DEFAULT" => "",
        ),
        "GROUP_EXACT_MATCH" => array(
            "NAME" => "Точное сравнение при фильтре \n выводимых групп",
            "PARENT" => "BASE",
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "N",
        ),
        "DEFAULT_GROUPS" => array(
            "NAME" => "Группы, выбранные по-умолчанию",
            "PARENT" => "BASE",
            "TYPE" => "LIST",
            "MULTIPLE" => "Y",
            "VALUES" => $arUserGroups,
            "ADDITIONAL_VALUES" => "Y",
            "DEFAULT" => "",
        ),
        "SUCCESS_URL" => array(
            "NAME" => "Страница, открываемая после успешного добавления",
            "PARENT" => "BASE",
            "TYPE" => "STRING",
            "DEFAULT" => ""
        ),
    ),
);
