<?php

class UsersAdd extends CBitrixComponent {
    protected $labels = [
        "LAST_NAME" => "Фамилия",
        "NAME" => "Имя",
        "SECOND_NAME" => "Отчество",
        "EMAIL" => "Email",
        "LOGIN" => "Логин",
        "WORK_COMPANY" => "Компания",
        "PASSWORD" => "Пароль",
        "PASSWORD_CONFIRM" => "Подтверждение пароля",
        "SEND_NOTIFICATION" => "Оповестить слушателя",
        "GROUP_ID" => "Группы",
    ];

    public function onPrepareComponentParams($arParams)
    {
        if (!$arParams['REQUIRED']) {
            $arParams['REQUIRED'] = [
                'EMAIL',
                'LOGIN',
                'PASSWORD',
                'PASSWORD_CONFIRM',
            ];
        }
        if (!$arParams['SHOW_GROUPS']) {
            $arParams['SHOW_GROUPS'] = [];
        }
        if (!$arParams['GROUP_EXACT_MATCH']) {
            $arParams['GROUP_EXACT_MATCH'] = "N";
        }
        
        return $arParams;
    }

    public function executeComponent()
    {
        $this->prepareResult();
        $this->processRequest();
        
        $this->includeComponentTemplate();
    }

    private function prepareResult()
    {
        $this->arResult["ERRORS"] = [];
        $this->arResult['USER'] = [
            'LAST_NAME' => '',
            'NAME' => '',
            'SECOND_NAME' => '',
            'EMAIL' => '',
            'LOGIN' => '',
            'GROUP_ID' => [],
        ];
        
        $this->arResult['GROUPS'] = $this->getGroups($this->arParams['SHOW_GROUPS'], $this->arParams['GROUP_EXACT_MATCH']);
        $this->arResult['GROUPS'] = $this->getSetDefaultGroups($this->arResult['GROUPS']);
    }

    private function getGroups($groupsCodes, $exactMatch = "N")
    {
        $groups = [];
        $rs = CGroup::GetList($by="ID", $order="ASC",
            array(
                "STRING_ID" => implode('|', $groupsCodes),
                "STRING_ID_EXACT_MATCH" => ($exactMatch == "N" || !$exactMatch) ? "N" : "Y", 
            ));
        while($ar = $rs->Fetch()) {
            $groups[] = $ar;
        }
        return $groups;
    }

    private function getSetDefaultGroups($arGroups)
    {
        foreach ($arGroups as &$arGroup) {
            if (in_array($arGroup["ID"], $this->arParams["DEFAULT_GROUPS"]) || in_array($arGroup["STRING_ID"], $this->arParams["DEFAULT_GROUPS"])) {
                $arGroup["DEFAULT"] = "Y";
            }
        }
        unset($arGroup);
        return $arGroups;
    }

    private function processRequest()
    {
        global $APPLICATION;
        try {
            $request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
        } catch (\Bitrix\Main\SystemException $e) {
            $this->arResult['ERRORS'][] = $e->getMessage();
            return;
        }
        if ($request->isPost() ){//&& check_bitrix_sessid()) {
            $errors = [];
            $arFields = $request->getPost('USER');
            foreach($request->getPost('USER') as $field => $value) {
                if (in_array($field, $this->arParams['REQUIRED']) && !$value) {
                    $errors[] = $this->getLabel($field) . " обязательно для заполнения";
                }
            }
            
            if (!$errors) {
                $obUser = new CUser();
                $arFields['ACTIVE'] = "Y";
                if ($UID = $obUser->Add($arFields)) {
                    if($request->getPost("user_info_event") == "Y")
                    {
                        if (true) { // is new
                            $text = "Администратор зарегистрировал вас на сайте";
                        } else {
                            $text = "Администратор сайта изменил ваши регистрационные данные.";
                        }
                        CUser::SendUserInfo($UID, SITE_ID, $text, true);
                    }
                    
                    LocalRedirect($this->arParams["SUCCESS_URL"] . "?msg=" . urlencode("Пользователь добавлен"));
                }
                $errors[] = $obUser->LAST_ERROR;
                if ($APPLICATION->GetException())
                {
                    $err = $APPLICATION->GetException();
                    $errors[] = $err->GetString();
                    $APPLICATION->ResetException();
                }
            }
            
            $this->arResult["ERRORS"] = $errors;
            $this->arResult["USER"] = $arFields;
        }
        
    }

    private function getLabel($field)
    {
        if(array_key_exists($field, $this->labels)) {
            return $this->labels[$field];
        }
        return $field;
    }
}