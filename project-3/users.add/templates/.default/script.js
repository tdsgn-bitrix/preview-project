
$(document).ready(function() {
    $('[name="USER[PERSONAL_BIRTHDAY]"]').inputmask("dd.mm.yyyy", {
        placeholder: 'дд.мм.гггг'
    });
    $('[name="USER[UF_PASSPORT_NUMBER]"]').inputmask("99 99 999999", {
        placeholder: '__ __ ______'
    });
    $('[name="USER[UF_PASSPORT_GIVEN]"]').inputmask({
        // mask: "dd.mm.yyyy *",
        mask: "1.2.y *{+}",
        placeholder: 'дд.мм.гггг ___',
        alias: 'dd/mm/yyyy',
        separator: ".",
        leapday: "29.02.",
        definitions: {
            '*': {
                validator: "[0-9A-Za-zА-яЁёÀ-ÿµ!#$%&'*+/=?^_`{|}~\\- \t]",
                casing: "lower",
                cardinality: 1
            }
        }
    });
});