<?
if (!function_exists('__CalendarDate')) {
    function __CalendarDate($sFromName, $sFromVal, $sFormName = "skform", $size = "10", $param = "class=\"typeinput\"")
    {
        return '<input type="text" name="' . $sFromName . '" id="' . $sFromName . '" size="' . $size . '" value="' . htmlspecialcharsbx($sFromVal) . '" ' . $param . ' /> ' . "\n"
            . '<div class="input-group-addon">'
            . Calendar($sFromName, $sFormName) . "\n"
            . '</div>';
    }
}
?>

    <?if($arResult["ERRORS"]):?>
    <div class="row bg-danger">
        <div class="col-xs-12">
            <p><?=implode('<br><br>', $arResult["ERRORS"])?></p>
        </div>
    </div>
    <?endif;?>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
<form name="user_edit_form" class="form-horizontal" method="post" action="<?=POST_FORM_ACTION_URI?>">
    <?=bitrix_sessid_post()?>
    <div class="form-group">
        <label for="input_work_company" class="col-sm-3 control-label">Компания</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="input_work_company" name="USER[WORK_COMPANY]" placeholder="Компания" value="<?=$arResult['USER']['WORK_COMPANY']?>">
        </div>
    </div>
    <div class="form-group">
        <label for="input_last_name" class="col-sm-3 control-label">Фамилия</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="input_last_name" name="USER[LAST_NAME]" placeholder="Фамилия" value="<?=$arResult['USER']['LAST_NAME']?>">
        </div>
    </div>
    <div class="form-group">
        <label for="input_name" class="col-sm-3 control-label">Имя</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="input_name" name="USER[NAME]" placeholder="Имя" value="<?=$arResult['USER']['NAME']?>">
        </div>
    </div>
    <div class="form-group">
        <label for="input_name" class="col-sm-3 control-label">Отчество</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="input_second_name" name="USER[SECOND_NAME]" placeholder="Отчество" value="<?=$arResult['USER']['SECOND_NAME']?>">
        </div>
    </div>
    <div class="form-group">
        <label for="input_email" class="col-sm-3 control-label">Email</label>
        <div class="col-sm-9">
            <input type="email" class="form-control" id="input_email" name="USER[EMAIL]" placeholder="Email" value="<?=$arResult['USER']['EMAIL']?>">
        </div>
    </div>
    <div class="form-group">
        <label for="input_login" class="col-sm-3 control-label">Логин</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="input_login" name="USER[LOGIN]" placeholder="Логин" value="<?=$arResult['USER']['LOGIN']?>">
        </div>
    </div>
    <div class="form-group">
        <label for="input_pass" class="col-sm-3 control-label">Пароль</label>
        <div class="col-sm-9">
            <input type="password" class="form-control" id="input_pass" name="USER[PASSWORD]" placeholder="Пароль">
        </div>
    </div>
    <div class="form-group">
        <label for="input_pass_confirm" class="col-sm-3 control-label">Подтверждение пароля</label>
        <div class="col-sm-9">
            <input type="password" class="form-control" id="input_pass_confirm" name="USER[PASSWORD_CONFIRM]" placeholder="Подтверждение пароля">
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-12 col-sm-4">
            <label for="input_notify" class="col-xs-10 control-label">Оповестить слушателя</label>
            <div class="col-xs-2">
                <label><input type="checkbox" id="input_notify" name="user_info_event" value="Y" <?=$_REQUEST['user_info_event'] == "Y" ? "checked" : ""?>></label>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <label for="input_forcefill" class="col-xs-10 control-label">Требовать пользователя заполнить профиль</label>
            <div class="col-xs-2">
                <input type="hidden" name="USER[UF_FORCEFILL_PROFILE]" value="0" >
                <label><input type="checkbox" id="input_forcefill" name="USER[UF_FORCEFILL_PROFILE]" value="1" <?= $arResult["USER"]["UF_FORCEFILL_PROFILE"] == "1" ? "checked" : "" ?>></label>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <label for="input_documents_done" class="col-xs-10 control-label">Сделаны документы</label>
            <div class="col-xs-2">
                <input type="hidden" name="USER[UF_DOCUMENTS_DONE]" value="0" >
                <label><input type="checkbox" id="input_documents_done" name="USER[UF_DOCUMENTS_DONE]" value="1" <?= $arResult["USER"]["UF_DOCUMENTS_DONE"] == "1" ? "checked" : "" ?>></label>
            </div>
        </div>
    </div>


    <div class="well">
        <div class="form-group">
            <label for="input_personal_agree" class="col-xs-10 col-sm-3 control-label">Согласие на обработку персональных данных</label>
            <div class="col-xs-2 col-sm-9">
                <input type="hidden" name="USER[UF_PERSONAL_AGREE]" value="0" >
                <label><input type="checkbox" id="input_personal_agree" name="USER[UF_PERSONAL_AGREE]" value="1" <?= $arResult["USER"]["UF_PERSONAL_AGREE"] == "1" ? "checked" : "" ?>></label>
            </div>
        </div>

        <div class="form-group">
            <label for="input_birthday" class="col-sm-3 control-label">Дата рождения</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="input_birthday" name="USER[PERSONAL_BIRTHDAY]" placeholder="дд.мм.гггг" value="<?=$arResult['USER']['PERSONAL_BIRTHDAY']?>">
            </div>
        </div>

        <div class="form-group">
            <label for="input_diploma" class="col-sm-3 control-label">Диплом об образовании</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="input_diploma" name="USER[UF_DIPLOMA]" placeholder="№" value="<?=$arResult['USER']['UF_DIPLOMA']?>">
            </div>
        </div>
        <div class="form-group">
            <label for="input_speciality" class="col-sm-3 control-label">Специализация</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="input_speciality" name="USER[UF_SPECIALITY]" placeholder="____________________________" value="<?=$arResult['USER']['UF_SPECIALITY']?>">
            </div>
        </div>
        <div class="form-group">
            <label for="input_pass_number" class="col-sm-3 control-label">Серия и номер паспорта</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="input_pass_number" name="USER[UF_PASSPORT_NUMBER]" placeholder="__ __ ______" value="<?=$arResult['USER']['UF_PASSPORT_NUMBER']?>">
            </div>
        </div>
        <div class="form-group">
            <label for="input_pass_given" class="col-sm-3 control-label">Когда и кем выдан</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="input_pass_given" name="USER[UF_PASSPORT_GIVEN]" placeholder="дд.мм.гггг ____________________________" value="<?=$arResult['USER']['UF_PASSPORT_GIVEN']?>">
            </div>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">Группы</label>
        <div class="col-sm-9">
            <?
            foreach ($arResult['GROUPS'] as $arGroup):
                $checkGroup = false;
                if (isset($arResult["USER"]["GROUP_ID"][$arGroup["ID"]])) {
                    $checkGroup = $arResult["USER"]["GROUP_ID"][$arGroup["ID"]]["GROUP_ID"] == "Y" || $arResult["USER"]["GROUP_ID"][$arGroup["ID"]]["GROUP_ID"] == $arGroup["ID"];
                } elseif($arGroup["DEFAULT"] == "Y") {
                    $checkGroup = true;
                }
                ?>
                <div class="row">
                    <div class="col-xs-12 checkbox">
                        <label <?if($arGroup["DESCRIPTION"]):?>title="<?=$arGroup["DESCRIPTION"]?>"<?endif?>>
                            <input type="checkbox" name="USER[GROUP_ID][<?=$arGroup["ID"]?>][GROUP_ID]" 
                                   value="<?=$arGroup["ID"]?>" <?=($checkGroup ? "checked" : "") ?>>
                            <?=$arGroup["NAME"]?>
                        </label>
                    </div>
                    <div class="col-xs-6">
                        <div class="input-group">
                            <div class="input-group-addon">с</div>
<!--                            <input type="date" class="form-control" name="USER[GROUP_ID][--><?//=$arGroup["ID"]?><!--][DATE_ACTIVE_FROM]" placeholder="Активность с"-->
<!--                                value="--><?//=$arResult["USER"]["GROUP_ID"][$arGroup["ID"]]["DATE_ACTIVE_FROM"]?><!--">-->
                            <?= __CalendarDate("USER[GROUP_ID][".$arGroup["ID"]."][DATE_ACTIVE_FROM]", ($checkGroup ? htmlspecialcharsbx($arResult["USER"]["GROUP_ID"][$arGroup["ID"]]["DATE_ACTIVE_FROM"]) : ""), "user_edit_form", "22", "class=\"form-control\"")?>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="input-group">
                            <div class="input-group-addon">по</div>
                            <?= __CalendarDate("USER[GROUP_ID][".$arGroup["ID"]."][DATE_ACTIVE_TO]", ($checkGroup ? htmlspecialcharsbx($arResult["USER"]["GROUP_ID"][$arGroup["ID"]]["DATE_ACTIVE_TO"]) : ""), "user_edit_form", "22", "class=\"form-control\"")?>
<!--                            <input type="date" class="form-control" name="USER[GROUP_ID][--><?//=$arGroup["ID"]?><!--][DATE_ACTIVE_TO]" placeholder="Активность по"-->
<!--                                value="--><?//=$arResult["USER"]["GROUP_ID"][$arGroup["ID"]]["DATE_ACTIVE_TO"]?><!--">-->
                        </div>
                    </div>
                </div>
            <?endforeach;?>
        </div>
    </div>
    
    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-9">
            <button type="submit" class="btn btn-default">Добавить</button>
        </div>
    </div>
</form>

    </div>
</div>
