<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?php if (!$arResult["ERROR"]) : ?>
    <div class="header-user-info">
        <div class="header-user-info__fio"><?php echo $arResult["FIO"]; ?></div>
        <div class="header-user-info__company"><?php echo $arResult["COMPANY"]; ?></div>
    </div>
<?php endif; ?>

