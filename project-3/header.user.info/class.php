<?php

class HeaderUserInfo extends CBitrixComponent {
    public function executeComponent()
    {
        global $USER;
        if ($USER->IsAuthorized()) {
            $arUser = CUser::GetByID($USER->GetID())->Fetch();
            $this->arResult["FIO"] = $arUser["LAST_NAME"] . " " . $arUser["NAME"] . " " . $arUser["SECOND_NAME"];
            $this->arResult["COMPANY"] = $arUser["WORK_COMPANY"];
            $this->arResult["ERROR"] = false;
        } else {
            $this->arResult["ERROR"] = true;
        }
        
        $this->includeComponentTemplate();
    }
}
