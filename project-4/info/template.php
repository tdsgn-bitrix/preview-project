<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
$n = 0;
$j = 0;

?>
<?php foreach ($arResult["ITEMS"] as $arItem):
    ?>
    <div class="t-information-disclosure__item t-animate-item">
        <div class="t-information-disclosure__head js-info-head">
            <p><?= $arItem['NAME'] ?></p>
            <div class="t-information-disclosure__plus"><span></span></div>
        </div>
        <?php if (!empty($arItem["PROPERTIES"]["IMG"]["VALUE"])) : ?>
            <div class="t-information-disclosure__content js-info-content">
                <div class="t-information-disclosure__img">
                    <img src="<?= CFile::GetPath($arItem["PROPERTIES"]["IMG"]["VALUE"]) ?>" alt="">
                </div>
            </div>
        <?php endif; ?>
        <?php if (!empty($arItem["PROPERTIES"]["TEXT"]["VALUE"])): ?>
            <div class="t-information-disclosure__content js-info-content">
                <p class=""><?= htmlspecialcharsBack($arItem["PROPERTIES"]["TEXT"]["VALUE"]["TEXT"]) ?></p>
            </div>
        <?php endif; ?>

        <?php if (!empty($arItem["PROPERTIES"]["DOC"]["VALUE"])): ?>
            <div class="t-information-disclosure__content js-info-content">


                <?php $VALUES = array();
                $res = CIBlockElement::GetProperty(13, $arItem['ID'], array("sort" => "desc"), array("CODE" => "DOC"));
                while ($ob = $res->GetNext()) {
                    $VALUES[] = $ob['VALUE'];
                }
                $rsProducts = CIBlockElement::GetList(array("SORT" => "ASC"), ['ID' => $VALUES], false, false, ['SORT', 'NAME', 'PROPERTY_DOC', 'PROPERTY_NAME', 'PROPERTY_PUBLIC',]);

                while ($arrProduct = $rsProducts->Fetch()) { ?>
                    <a href="<?= CFile::GetPath($arrProduct["PROPERTY_DOC_VALUE"]) ?>" target="_blank"
                       class="t-info-block__info" style="order: <?= $arrProduct['SORT']; ?>">
                        <div class="t-info-block__img">
                            <!--                            <img src="/img/icons/pdf.svg" alt="">-->
                        </div>
                        <div class="t-info-block__right">
                            <p class="t-info-block__link"><?= $arrProduct["PROPERTY_NAME_VALUE"] ?></p>
                            <span class="t-info-block__subtext"><?= $arrProduct["PROPERTY_PUBLIC_VALUE"] ?></span>
                        </div>
                    </a>
                    <?php
                }
                ?>

            </div>
        <?php endif; ?>
        <?php if (!empty($arItem["PROPERTIES"]["FILES"]["VALUE"])) : ?>

            <div class="t-information-disclosure__content js-info-content">
                <div class="t-info-block js-projects-block">
                    <div class="t-info-block__wrap">
                        <div class="t-info-block__container">
                            <?php
                            $min = 0;
                            foreach ($arItem["PROPERTIES"]["FILES"]["VALUE"] as $arSect) {

                                $arSections = CIBlockSection::GetByID($arSect);
                                $ar_res = $arSections->GetNext();
                                if ($min == 0) {
                                    $min = (int)$ar_res["SORT"];
                                } else {
                                    $min = (int)$ar_res["SORT"] > $min ? $min : (int)$ar_res["SORT"];
                                }
                            }
                            ?>

                            <?php foreach ($arItem["PROPERTIES"]["FILES"]["VALUE"] as $arSect) :
                                $arSections = CIBlockSection::GetByID($arSect);
                                $ar_res = $arSections->GetNext(); ?>

                                <div class="t-info-block__item js-projects-item <?php if ($ar_res["SORT"] == $min) : ?> _active <?php else: ?> <?php endif; ?>"
                                     data-block="<?= $n++ ?>" style="order:<?= $ar_res["SORT"]; ?>">
                                    <span><?= $ar_res["NAME"]; ?></span>
                                </div>
                            <?php endforeach; ?>

                        </div>
                    </div>
                    <div class="t-info-block__content js-projects-content">

                        <?php foreach ($arItem["PROPERTIES"]["FILES"]["VALUE"] as $arSect) :
                            $arSections = CIBlockSection::GetByID($arSect);
                            $ar_res = $arSections->GetNext();
                            $arSelect = array("ID", "IBLOCK_ID", "NAME", "PROPERTY_*", "SECTION_ID", "SORT");
                            $arFilter = array("IBLOCK_ID" => 14, "SECTION_ID" => $ar_res['ID']);
                            $resElements = CIBlockElement::GetList(array("SORT" => "ASC"), $arFilter, false, false, $arSelect); ?>
                            <div class="t-info-block__text js-projects-text <?php if ($ar_res["SORT"] != $min) : ?> _hidden-block <?php else: ?> <?php endif; ?>"
                                 data-block="<?= $j++ ?>" style="order: <?= $ar_res["SORT"]; ?>">
                                <?php while ($obElements = $resElements->GetNextElement()) {
                                    $arFields = $obElements->GetFields();
                                    $arProps = $obElements->GetProperties();
                                    ?>
                                    <a href="<?= CFile::GetPath($arProps["FILE"]["VALUE"]); ?> "
                                       target="_blank"
                                       class="t-info-block__info">
                                        <div class="t-info-block__img">
                                        </div>
                                        <div class="t-info-block__right">
                                            <p class="t-info-block__link"><?= $arProps["NAME"]["VALUE"] ?></p>
                                            <span class="t-info-block__subtext"><?= $arrProduct["PUBLIC"]["VALUE"] ?></span>
                                        </div>
                                    </a>
                                <?php } ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>


    </div>
<?php endforeach; ?>
