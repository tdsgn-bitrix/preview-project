<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$dbResSect = CIBlockSection::GetList(
    array("SORT" => "ASC"),
    array("IBLOCK_CODE" => 'info', "ACTIVE" => "Y")
);
while ($sectRes = $dbResSect->GetNext()) {
    $arSections[] = $sectRes;
}
foreach ($arSections as $arSection) {

    foreach ($arResult["ITEMS"] as $key => $arItems) {

        if ($arItems['IBLOCK_SECTION_ID'] == $arSection['ID']) {
            $arSection['ELEMENTS'][] = $arItems;
        }
    }
    $arElementGroups[] = $arSection;
}
$arResult["SECTIONS"] = $arElementGroups;


