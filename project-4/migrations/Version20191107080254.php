<?php

namespace Sprint\Migration;


class Version20191107080254 extends Version
{
    protected $description = "Элементы инфоблока дополнительная информация";

    public function up()
    {
        $helper = new HelperManager();

        $this->addSetting(
            $helper,
            [
                'NAME' => 'Миссия проекта',
                "CODE" => 'mission',
                'SORT' => 10,
                'PROPERTY_VALUES' => [
                    'TITLE' => 'Миссия проекта',
                ]
            ]
        );
        $this->addSetting(
            $helper,
            [
                'NAME' => 'Видение будущего',
                "CODE" => 'future',
                'SORT' => 20,
                'PROPERTY_VALUES' => [
                    'TITLE' => 'Видение будущего',
                ]
            ]
        );
        $this->addSetting(
            $helper,
            [
                'NAME' => 'Ценности',
                "CODE" => 'values',
                'SORT' => 30,
                'PROPERTY_VALUES' => [
                    'TITLE' => 'Ценности',
                ]
            ]
        );
        $this->addSetting(
            $helper,
            [
                'NAME' => 'Принципы работы',
                "CODE" => 'job',
                'SORT' => 40,
                'PROPERTY_VALUES' => [
                    'TITLE' => 'Принципы работы',
                ]
            ]
        );

    }

    public function down()
    {
        $helper = new HelperManager();

    }

    protected function addSetting($helper, $fields, $props = [])
    {
        $id = $helper->Iblock()
            ->updateElementIfExists(2, $fields, $props);

        if (false === $id) {
            $id = $helper->Iblock()
                ->addElement(2, $fields, $props);

            $this->out('Создан элемент [%s]', $id);
        }

        return $id;
    }
}