<?php

use Bitrix\Main\Application;
use Bitrix\Main\Page\Asset;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */

if ($arResult["TOP_IMAGE"]) {
	$APPLICATION->SetPageProperty("main_class", "");
	$APPLICATION->SetPageProperty("top_image", $arResult["TOP_IMAGE"]);
}
if ($arResult['DESCRIPTION']) {
	$APPLICATION->SetPageProperty("subtitle", $arResult['DESCRIPTION']);
}

if ($arResult['SECTION_PAGE_URL']) {
	Asset::getInstance()->addString('<link rel="canonical" href="' . 'https://' . Application::getInstance()->getContext()->getServer()->getServerName() . $arResult['SECTION_PAGE_URL'] . '">');
}

$APPLICATION->SetPageProperty("page_number", $arResult['page_number']);

if ($arResult['PAGEN'] > $arResult['page_count'])
{
    if (Bitrix\Main\Loader::includeModule("iblock"))
    {
        Bitrix\Iblock\Component\Tools::process404(
	        trim($arParams['MESSAGE_404']) ?: '404 Error',
	        true,
	        $arParams['SET_STATUS_404'] === 'Y',
	        $arParams['SHOW_404'] === 'Y',
	        $arParams['FILE_404']
        );
    }
    else
    {
        LocalRedirect('/404.php');
    }
   CMain::FinalActions();
   die();
}