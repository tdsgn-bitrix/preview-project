<?php

use Bitrix\Main\Localization\Loc;
use Gpart\Local\Helper;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$rnd = $this->randString();

$arItems = [];
$arSubCategories = [];


foreach ($arResult['SECTIONS'] as $SECTION) {
    $arSubCategories[] = [
        "heading" => $SECTION['NAME'],
        'href' => $SECTION['SECTION_PAGE_URL']
    ];
}

$text_column_catalog = false;



$twigData = [
    "text_column" => [
        "title" => $arResult["~UF_LEFT_LABEL"],
        "title_size" => "h2",
        "lead" => $arResult["~UF_LEFT_TEXT"],
        "text" => $arResult["~UF_FEATURES"],
    ],
    "text_column_desc" => [
        "subtitle" => $arResult["~UF_ADV_TITLE"],
        "text" => $arResult["~UF_ADV_TEXT"],
    ],
    "text_column_catalog" => $text_column_catalog
];

$twigData["subcategories"] = [
    'heading' => 'Каталог',
    "items" => $arSubCategories
];

$arResult['TOP_IMAGE'] = '';
if ($arResult['SECTION']["DETAIL_PICTURE"]) {
    $arResult['TOP_IMAGE'] = CFile::GetPath($arResult['SECTION']["DETAIL_PICTURE"]);
}
$arResult["DESCRIPTION"] = $arResult['SECTION']["DESCRIPTION"];

$arResult["TEMPLATE_DATA"] = $twigData;


$this->__component->setResultCacheKeys(['page_number', 'page_count', 'PAGEN', 'TOP_IMAGE', 'DESCRIPTION', 'SECTION_PAGE_URL']);

unset($twigData);