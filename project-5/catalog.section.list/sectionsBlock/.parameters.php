<?php

use Tdsgn\Core\Components\TemplateBlock;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

/**
 * @global CMain $APPLICATION
 * @var array    $arParams
 * @var array    $arCurrentValues
 * @var array    $arResult
 */


CBitrixComponent::includeComponentClass("tdsgn.core:template.block");


$arParams = [
	"GUARANTEE_LINK" => ["NAME" => "Ссылка на страницу \"Гарантии\""],
	"DEF_LINK"       => ["NAME" => "Ссылка на страницу \"Защита от гарантии\""]
];


$arTemplateParameters = TemplateBlock::initParameters($arParams);
