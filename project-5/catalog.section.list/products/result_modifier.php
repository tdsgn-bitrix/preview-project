<?php

use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$rnd = $this->randString();

$productList = &$arResult['TEMPLATE_DATA']['product_list'];

$productList = false;

if ($arResult['SECTIONS']) {
	
	$strSectionEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'SECTION_EDIT');
	$strSectionDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'SECTION_DELETE');
	$arSectionDeleteParams = ['CONFIRM' => Loc::getMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM')];
	
	foreach ($arResult['SECTIONS'] as $arSection) {
		$entryId = $arSection['ID'] . $rnd;
		$this->AddEditAction($entryId, $arSection['EDIT_LINK'], $strSectionEdit);
		$this->AddDeleteAction($entryId, $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
		
		
		$image = false;
		if ($pic = $arSection['PICTURE']) {
			$image = [
				'src' => $arSection['PICTURE']['SRC'],
				'alt' => $arSection['PICTURE']['DESCRIPTION'] ?: $arSection['NAME']
			];
			if ($fileId = $arSection['UF_PICTURE_MOBILE']) {
				$image['mob'] = [
					'srcset' => [
						[
							'scale' => 1,
							'src'   => CFile::GetPath($fileId)
						]
					]
				];
			}
		}
		
		$item = [
			'attr' => 'id="' . ($this->GetEditAreaId($entryId)) . '"',
			
			'href'  => $arSection['SECTION_PAGE_URL'],
			'title' => $arSection['~UF_NAME_HTML'] ?: $arSection['NAME'],
			'image' => $image
		];
		
		$productList['items'][] = $item;
	}
	
}

if ($link = trim($arParams['GUARANTEE_LINK'])) {
	$productList['items'][] = [
		'image_icon' => true,
		'href'       => $link,
		'title'      => Loc::getMessage('GUARANTEE'),
		'icon'       => [
			'name' => '64/garanty'
		]
	];
}

if ($link = trim($arParams['DEF_LINK'])) {
	$productList['items'][] = [
		'image_icon' => true,
		'href'       => $link,
		'title'      => Loc::getMessage('COUNTERFEIT_PROTECTION'),
		'icon'       => [
			'name' => '64/def'
		]
	];
}
